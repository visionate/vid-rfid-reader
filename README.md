
## get USB readers

    # raspberryPI
    lsusb | grep "metraTec"

    # find ports
    . shell/getUSB.sh

    #mac
    ls -al /dev | grep "usb"

    # Windows
    Device Manager
    COM Ports



## serial usb on synology

    # check usb
    lsusb
        |__usb1          1d6b:0002:0310 09  2.00  480MBit/s 0mA 1IF  (ehci_hcd 0000:00:16.0) hub
          |__1-1         8087:07db:0003 09  2.00  480MBit/s 0mA 1IF  ( ffffffd1ffffffb2ffffffdbffffffad) hub
            |__1-1.1     f400:f400:0100 00  2.00  480MBit/s 200mA 1IF  (Synology DiskStation 7F0086DAB2DA5A66)
        |__usb2          1d6b:0002:0310 09  2.00  480MBit/s 0mA 1IF  (Linux 3.10.105 etxhci_hcd-170202 Etron xHCI Host Controller 0000:04:00.0) hub
          |__2-1         1a40:0201:0100 09  2.00  480MBit/s 100mA 1IF  ( ffffffe9ffffffbdffffffe9ffffffa4) hub
            |__2-1.1     0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DFNJTIHB)
            |__2-1.2     0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DF1SZ99F)
            |__2-1.3     0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DFPJQYZQ)
            |__2-1.4     1a40:0101:0111 09  2.00  480MBit/s 100mA 1IF  ( ffffffd6ffffffa3ffffffebffffffcb) hub
              |__2-1.4.1 0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DFKA5HNF)
              |__2-1.4.2 0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DF9UH4GQ)
              |__2-1.4.3 0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DFW3QNUN)
              |__2-1.4.4 0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DFYKZV6M)
            |__2-1.7     0403:6001:0600 00  2.00   12MBit/s 100mA 1IF  (metraTec GmbH metraTec DeskID MF Reader DFGGHLNO)
        |__usb3          1d6b:0003:0310 09  3.00 5000MBit/s 0mA 1IF  (Linux 3.10.105 etxhci_hcd-170202 Etron xHCI Host Controller 0000:04:00.0) hub

    # install drivers
    sudo insmod /lib/modules/usbserial.ko
    sudo insmod /lib/modules/ftdi_sio.ko

    dmesg | grep tty
        [3868320.479455] usb 2-1.1: FTDI USB Serial Device converter now attached to ttyUSB0
        [3868320.522583] usb 2-1.2: FTDI USB Serial Device converter now attached to ttyUSB1
        [3868320.565907] usb 2-1.3: FTDI USB Serial Device converter now attached to ttyUSB2
        [3868320.609168] usb 2-1.7: FTDI USB Serial Device converter now attached to ttyUSB3
        [3868320.653575] usb 2-1.4.1: FTDI USB Serial Device converter now attached to ttyUSB4
        [3868320.698268] usb 2-1.4.2: FTDI USB Serial Device converter now attached to ttyUSB5
        [3868320.742910] usb 2-1.4.3: FTDI USB Serial Device converter now attached to ttyUSB6
        [3868320.787581] usb 2-1.4.4: FTDI USB Serial Device converter now attached to ttyUSB7

    ls -al /sys/bus/usb-serial/drivers/ftdi_sio/
        ttyUSB0 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.1/2-1.1:1.0/ttyUSB0
        ttyUSB1 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.2/2-1.2:1.0/ttyUSB1
        ttyUSB2 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.3/2-1.3:1.0/ttyUSB2
        ttyUSB3 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.7/2-1.7:1.0/ttyUSB3
        ttyUSB4 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.4/2-1.4.1/2-1.4.1:1.0/ttyUSB4
        ttyUSB5 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.4/2-1.4.2/2-1.4.2:1.0/ttyUSB5
        ttyUSB6 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.4/2-1.4.3/2-1.4.3:1.0/ttyUSB6
        ttyUSB7 -> ../../../../devices/pci0000:00/0000:00:04.0/0000:04:00.0/usb2/2-1/2-1.4/2-1.4.4/2-1.4.4:1.0/ttyUSB7

## autostart drivers

Edit or create the file startup.sh via:

    sudo vi /usr/local/etc/rc.d/startup.sh
    sudo nano /usr/local/etc/rc.d/startup.sh

Put the next text into the file:

    sudo insmod /lib/modules/usbserial.ko
                                               sudo insmod /lib/modules/ftdi_sio.ko
                                               sudo chmod 777 /lib/modules/usbserial.ko
                                               sudo chmod 777 /lib/modules/ftdi_sio.ko

Change rights of startup.sh:

    sudo chmod 700 /usr/local/etc/rc.d/startup.sh


## mount serial devices DOCKER

    /dev is auto mounted into container when priviliged

    ls -al /dev/tty*

    dmesg | grep tty
    usb 1-1: FTDI USB Serial Device converter now attached to ttyUSB0

    ls -al /sys/bus/usb-serial/drivers/ftdi_sio/

    # devices
    /dev/ttyUSB0 .. 7
