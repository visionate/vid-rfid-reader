'use strict';
Object.defineProperty(exports, '__esModule', {value: true});
var Logger_1 = require('./lib/com/visionate/logging/Logger');
var rfid_reader_manager_1 = require('./lib/rfid-reader-manager');
var logger = Logger_1.CLog.createClogger('rfid-reader');
var rfidReaderManager;
process.on('uncaughtException', function (err) {
    logger.error('uncaughtException:%O', err);
});
var shutdownInProgress = false;
process.on('SIGINT' || 'SIGTERM', function () {
    logger.info('Got SIGINT | SIGTERM. Graceful shutdown pid: %s', process.pid);
    if (rfidReaderManager && shutdownInProgress === false) {
        rfidReaderManager
            .shutdown()
            .then(function () {
                return void 0;
            })
            .catch(function (e) {
                return process.exit(1);
            });
        shutdownInProgress = true;
    }
});
if (process.env.READERCONFIG) {
    logger.info('process.env.READERCONFIG: %o', process.env.READERCONFIG);
    var configPath = process.env.READERCONFIG;
    var config = require(configPath);
    if (config) {
        var readerConfig = config.reader;
        delete config.reader;
        if (process.env.READER_ID) {
            readerConfig.id = process.env.READER_ID;
        }
        if (process.env.READER_PORTPATH) {
            readerConfig.portPath = process.env.READER_PORTPATH;
        }
        logger.info('config: %o', config);
        logger.info('readerConfig: %o', readerConfig);
        initReader(readerConfig, config);
    } else {
        logger.error('no config at configPath: %s', configPath);
    }
}

function initReader(readerConfig, config) {
    logger.info('initReader config: %o', config);
    rfidReaderManager = new rfid_reader_manager_1.RfidReaderManager(readerConfig, config);
}
