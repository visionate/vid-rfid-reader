module.exports = {
  apps: [{
    name: 'cinema1-rfid',
    script: './run-rfid-reader.js',
    log: './logs/cinema1-rfid-log-error.log',
    logDateFormat: 'DD.MM.-HH:mm:ss.SSS',
    restart_delay: 5000,
    env: {
      NODE_ENV: 'production',
      LOGNAME: 'vid:rfid-reader',
      DEBUG: 'vid:rfid-reader:error:*,vid:rfid-reader:warn:*,vid:rfid-reader:log:*',
      DEBUG_HIDE_DATE: 'true',
      READERCONFIG: './config-rfid.json',
      READER_ID: 'cinema1',
      READER_PORTPATH: '/dev/tty.usbserial-DFOEA6ZA'
    }
  }]
};
