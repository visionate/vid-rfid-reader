import * as _ from 'lodash';
import * as request from 'request';
import {CLog, CLogger} from './com/visionate/logging/Logger';
import {IRFIDEvent, ITags, RfidReaderController} from './controller/rfid-reader.controller';

export class RfidReaderManager {
  public logger: CLogger = CLog.createClogger( 'RfidReaderManager' );

  public rfidController: RfidReaderController;

  protected continuesINV: boolean = false;

  protected readTagDataAfterINV: boolean = false;

  constructor( readerConfig, config ) {
    this.initRFID( readerConfig, config );
  }

  public test_writeToCard( data: any ) {
    this.logger.info( 'writeToCard ' );
    this.rfidController.inventory()
        .then( ( inv ) => {
          this.logger.info( 'writeToCard => inventory %o', inv );
          if ( inv !== {} ) {
            const tag = _.head( _.keys( inv ) );
            this.rfidController.writeToDataToCard( tag, JSON.stringify( data ) )
                .then( () => {
                  this.logger.info( 'writing Card Sucessfull!' );
                  this.inventory();
                } )
                .catch( e => this.logger.error( 'writeToCard error', e ) );
          }
        } )
        .catch( e => this.logger.error( 'writeToCard %O', e ) );
  }

  public writeToCard( tagID: string, data: string ): Promise<void> {
    this.logger.info( 'writeToCard %s data:%o', tagID, data );

    let stringData;

    try {
      stringData = JSON.stringify( data );
    } catch ( e ) {
      return Promise.reject( e );
    }

    return this.rfidController.writeToDataToCard( tagID, stringData );
  }

  public inventory(): Promise<ITags> {
    this.logger.info( 'inventory ' );
    return this.rfidController.inventory();
  }

  public readTagData( tagID: string ): Promise<string> {
    this.logger.info( 'readTagData %s', tagID );
    return this.rfidController.readTagData( tagID );
  }

  private initRFID( readerConfig, config ) {

    this.logger.info( 'initRFID readerConfig: %o', readerConfig );
    this.logger.info( 'initRFID config: %o', config );

    this.continuesINV = readerConfig.continuesINV === true || readerConfig.continuesINV === 'true';
    this.readTagDataAfterINV = readerConfig.readTagDataAfterINV === true || readerConfig.readTagDataAfterINV === 'true';

    // rfidController
    this.rfidController = new RfidReaderController( readerConfig );
    RfidReaderController.noReaderTimeoutMS = config.noReaderTimeoutMS;
    RfidReaderController.restartINVTimeoutMS = config.restartINVTimeoutMS;
    RfidReaderController.commandTimeoutMS = config.commandTimeoutMS;

    this.rfidController.on( 'ready', ( event: Event ) => {
      const evt = 'ready: ' + readerConfig.id + ' port:' + readerConfig.portPath;
      this.logger.info( 'ready %o', evt );
      if ( this.continuesINV === true ) {
        this.rfidController.startContinuesInventory( this.readTagDataAfterINV );
      }
    } );
    this.rfidController.on( RfidReaderController.tagAdd, ( event: IRFIDEvent ) => {
      const url = config.rfidEventURL + '?tagAdd=true';
      this.logger.info( 'tagAdd %s %o', url, event );
      this.postRequest( event, url );
    } );
    this.rfidController.on( RfidReaderController.tagRemove, ( event: IRFIDEvent ) => {
      const url = config.rfidEventURL + '?tagRemove=true';
      this.logger.info( 'tagRemove %s %o', url, event );
      this.postRequest( event, url );
    } );
    this.rfidController.on( 'close', ( event: Event ) => {
      const evt = 'close: ' + readerConfig.id + ' port:' + readerConfig.portPath;
      this.logger.info( 'close %o', evt );
    } );
    this.rfidController.on( 'error', err => {
      this.logger.error( 'rfidController on Error: %O', err );
    } );
  }

  private postRequest( val, uri ) {
    try {
      request.post( {
        url: uri,
        json: true,
        body: val
      }, ( err, res, body ) => {
        if ( err ) {
          this.logger.error( err );
        }
      } );
    } catch ( er ) {
      this.logger.error( er );
    }
  }

  public shutdown(): Promise<void> {
    this.logger.info( 'shutdown()' );
    if ( this.rfidController ) {
      return this.rfidController.disconnect();
    } else {
      Promise.resolve();
    }
  }
}
