import debug0 from 'debug';

if (!process.env.LOGNAME) {
  process.env.LOGNAME = 'vid:rfid-reader';
}
let debug = debug0(process.env.LOGNAME);

let error = debug.extend('error');

let warn = debug.extend('warn');
warn.log = console.warn.bind(console);

let log = debug.extend('log');
log.log = console.info.bind(console);

let trace = debug.extend('trace');
trace.log = console.debug.bind(console);

export class CLog {
  public static createClogger<TA>(name: string): CLogger {
    return new CLogger(name);
  }
}

export class CLogger {
  private logger;

  private warner;

  private tracer;

  private errorLogger;

  constructor(name) {
    this.errorLogger = error.extend(name);
    this.logger = log.extend(name);
    this.warner = warn.extend(name);
    this.tracer = trace.extend(name);
  }

  public trace(...otherParams: any[]): void {
    this.tracer(...otherParams);
  }

  public log(...otherParams: any[]): void {
    this.logger(...otherParams);
  }

  public info(...otherParams: any[]): void {
    this.logger(...otherParams);
  }

  public warn(...otherParams: any[]): void {
    this.warner(...otherParams);
  }

  public error(...otherParams: any[]): void {
    this.errorLogger(...otherParams);
  }
}
