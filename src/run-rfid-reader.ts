import { CLog, CLogger } from './com/visionate/logging/Logger';
import { RfidReaderManager } from './rfid-reader-manager';

const logger: CLogger = CLog.createClogger( 'rfid-reader' );

let rfidReaderManager: RfidReaderManager;

process.on( 'uncaughtException', function ( err ) {
  logger.error( 'uncaughtException:%O', err );
} );

let shutdownInProgress: boolean = false;
process.on( 'SIGINT' || 'SIGTERM', () => {
  logger.info( 'Got SIGINT | SIGTERM. Graceful shutdown pid: %s', process.pid );
  if ( rfidReaderManager && shutdownInProgress === false ) {
    rfidReaderManager
      .shutdown()
      .then( () => void 0 ) // process.exit( 0 )
      .catch( e => process.exit( 1 ) );

    shutdownInProgress = true;

  }
} );

if ( process.env.READERCONFIG ) {
  logger.info( 'process.env.READERCONFIG: %o', process.env.READERCONFIG );
  const configPath = process.env.READERCONFIG;

  const config = require( configPath );
  if ( config ) {
    const readerConfig = config.reader;
    delete config.reader;

    if ( typeof process.env.READER_ID !== 'undefined' ) {
      readerConfig.id = process.env.READER_ID;
    }
    if ( typeof process.env.READER_PORTPATH !== 'undefined' ) {
      readerConfig.portPath = process.env.READER_PORTPATH;
    }
    if ( typeof process.env.CONTINUES_INV !== 'undefined' ) {
      readerConfig.continuesINV = process.env.CONTINUES_INV;
    }
    if ( typeof process.env.READTAGDATA_AFTER_INV !== 'undefined' ) {
      readerConfig.readTagDataAfterINV = process.env.READTAGDATA_AFTER_INV;
    }

    logger.info( 'config: %o', config );
    logger.info( 'readerConfig: %o', readerConfig );

    initReader( readerConfig, config );
  } else {
    logger.error( 'no config at configPath: %s', configPath );
  }
}

function initReader( readerConfig, config ) {
  logger.info( 'initReader config: %o', config );
  rfidReaderManager = new RfidReaderManager( readerConfig, config );
}
