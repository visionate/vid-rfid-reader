import * as events from 'events';
import * as _ from 'lodash';
import * as SerialPort from 'serialport';
import { OpenOptions } from 'serialport';
import { CLog, CLogger } from '../com/visionate/logging/Logger';
import Delimiter = SerialPort.parsers.Delimiter;
import Readline = SerialPort.parsers.Readline;

export interface IRFIDEvent {
  id: string;
  tag: string;
  data?: string;
  hexdata?: any;
}

export interface ITags {
  [ key: string ]: string;
}

export interface IRfidReaderController extends events.EventEmitter {
  emit( event: 'open' | 'ready' | 'close' | 'error', evt: Event ): boolean;

  emit( event: 'tagAdd' | 'tagRemove', rfidEvent: IRFIDEvent ): boolean;

  on( event: 'open' | 'ready' | 'close' | 'error', listener: ( evt: Event ) => void ): this;

  on( event: 'tagAdd' | 'tagRemove', listener: ( rfidEvent: IRFIDEvent ) => void ): this;
}

export class RfidReaderController extends events.EventEmitter implements IRfidReaderController {

  public static logger: CLogger = CLog.createClogger( 'RfidReaderController' );

  public static tagAdd: string = 'tagAdd';

  public static tagRemove: string = 'tagRemove';

  public static myfareKey: string = 'FFFFFFFFFFFF';

  public static myfareEmptyBlock: string = '00000000000000000000000000000000';

  public static myfareKeyType: string = 'A';

  public static myfareNumberOfBlocks: number = 63;

  public static commandTimeoutMS: number = 2000;

  public static checkINVAliveIntervalMS: number = 15000;

  public static noReaderTimeoutMS: number = 5000;

  public static invDeadTimeoutMS: number = 12000;

  public static restartINVTimeoutMS: number = 500;

  public static maxReadAttempts: number = 5;

  public static maxWriteAttempts: number = 5;

  public static rwRetryTimeout: number = 280;

  public static codes = {
    error: {
      NMA: 'No MiFare classic chip Authenticated',
      WDL: 'Wrong Data Length of hex-string-parameter',
      NDB: 'No Data Block',
      KBR: 'Key B is Readable',
      ONE: 'Operation Not Executed',
      BME: 'Block Mode Error, not 0 or 3 (not writeable with value block function)',
      BNW: 'Block Not Writable',
      BAE: 'Block Access Error',
      BNA: 'Block Not Authenticated',
      AKW: 'Access bits or Keys not Writable',
      UKB: 'Use Key B for authentication',
      UKA: 'Use Key A for authentication',
      KNC: 'Keys not changeable',
      BIH: 'Block is too high (i.e. bigger than 63 at MiFare 1k)',
      ATE: 'Authentication Error (i.e. wrong key)',
      NKS: 'No Key Select, select a temporary or a static key',
      CNS: 'Card is Not Selected',
      NB0: 'Number of Blocks to Read is 0',

      UPA: 'Unknown parameter',
      EDX: 'A decimal parameter includes non decimal characters',

      ACE: 'Access Error',
      ARH: 'Antenna Reflectivity High',
      BOD: 'Brownout Detected',
      BOF: 'Buffer Overflow',
      CCE: 'Communication CRC Error',
      CER: 'CRC error',
      CRT: 'Command Receive Timeout',
      DNS: 'Did Not Sleep',
      EHF: 'Error Hardware Failure',
      EHX: 'Error Hexadecimal Expected',
      FLE: 'FIFO Length Error',
      HBE: 'Header Bit Error',
      NCM: 'Not in CNR Mode',
      NOR: 'Number Out of Range',
      NOS: 'Not Supported',
      NRF: 'No RF-Field Active',
      NSS: 'No Standard Selected',
      PDE: 'Preamble Detect Error',
      PFE: 'Prefix Error',
      PLE: 'PLL Error',
      RDL: 'Read Data too Long',
      RTU: 'Error Resource temporarily unavailable Cannot lock port',
      RXE: 'Response Length not as Expected Error',
      SRT: 'Watchdog Reset',
      TCE: 'Tag Communication Error',
      TMT: 'Too Many Tags',
      TNR: 'Tag Not Responding',
      TOE: 'TimeOut Error',
      TOR: 'Tag Out of Range',
      UCO: 'Unknown Command',
      UER: 'Unknown Error',
      URE: 'UART Receive Error',
      WMO: 'Wrong Mode'
    }
  };

  public static npmName = require( '../../package.json' ).name;

  public static npmVersion = require( '../../package.json' ).version;

  public trailerblocks: number[];

  public dataBlocks: number[];

  public id;

  public readerInitialised: boolean = false;

  protected port: SerialPort;

  protected parser: Readline;

  // protected tags: ITags = {};

  protected tagCache: ITags = {};

  protected lastTags: ITags = {};

  protected continuesINV: boolean = false;

  public readTagDataAfterINV: boolean = true;

  protected lastLogDate: Date = new Date();

  protected parserCallback: Function;

  public static hex_to_ascii( str1 ) {
    const hex = str1.split( '00' ).join( '' );
    let str = '';
    for ( let n = 0; n < hex.length; n += 2 ) {
      str += String.fromCharCode( parseInt( hex.substr( n, 2 ), 16 ) );
    }
    return str;
  }

  public static ascii_to_hex( str ) {
    const arr1 = [];
    for ( let n = 0, l = str.length; n < l; n++ ) {
      let hex = Number( str.charCodeAt( n ) ).toString( 16 );
      arr1.push( hex );
    }
    return arr1.join( '' );
  }

  constructor( readerConfig: any ) {
    super();

    RfidReaderController.logger.info( '%s:%s', RfidReaderController.npmName, RfidReaderController.npmVersion );

    const numSectors = 16;
    this.trailerblocks = [];
    this.dataBlocks = [];
    for ( let i = 1; i <= numSectors; i++ ) {
      const tb = (i * 4) - 1;
      this.dataBlocks.push( tb - 3 );
      this.dataBlocks.push( tb - 2 );
      this.dataBlocks.push( tb - 1 );
      this.trailerblocks.push( tb );
    }
    // 0 wieder raus
    this.dataBlocks.shift();

    // RfidReaderController.logger.info( 'trailerblocks %o', this.trailerblocks );
    // RfidReaderController.logger.info( 'dataBlocks %o', this.dataBlocks );

    if ( readerConfig ) {
      this.id = readerConfig.id;
      setTimeout( () => {
        this.initSerialPort( readerConfig.portPath, readerConfig.config );
      }, 28 );
    } else {
      throw new Error( 'please provide readerConfig' );
    }
  }

  public listPorts() {
    RfidReaderController.logger.info( 'listPorts' );

    SerialPort.list()
              .then( ports => {
                if ( ports.length === 0 ) {
                  console.warn( 'No ports discovered' );
                }
                const headers = Object.keys( ports[ 0 ] );
                RfidReaderController.logger.info( JSON.stringify( headers ) );
                ports.forEach( ( port, index, portsArr ) => {
                  RfidReaderController.logger.info( 'PORT: %O', port );
                } );
              } )
              .catch( error => {
                RfidReaderController.logger.error( error );
              } );
  }

  /**
   * RST
   * The reset command resets the reader. The Reset command has no parameters. After sending the RST command the HF power is
   * turned off and the reader has to be initialized again.
   * */

  public resetReader(): Promise<void> {
    return new Promise( ( resolve, reject ) => {
      RfidReaderController.logger.info( 'resetReader' );

      const timeout = setTimeout( () => {
        this.parserCallback = void 0;
        reject( 'resetReader:Timeout' );
      }, RfidReaderController.commandTimeoutMS );

      this.parserCallback = ( res ) => {
        const resString = res.toString();
        RfidReaderController.logger.info( 'resetReader: %s %o', this.id, resString );
        if ( resString.indexOf( 'OK' ) > -1 ) {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          setTimeout( resolve, 250 );
        } else {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          reject( 'resetReader:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
        }
      };

      // RST Resets the Reader
      const command = 'RST';
      this.sendSerialCommand( command );
    } );
  }

  public startContinuesInventory( readTagDataAfterINV = false ) {
    RfidReaderController.logger.info( 'startContinuesInventory %s readTagDataAfterINV:%s', this.id, readTagDataAfterINV );
    this.continuesINV = true;
    this.readTagDataAfterINV = readTagDataAfterINV;
    this.runINV();
  }

  public inventory(): Promise<ITags> {
    RfidReaderController.logger.info( 'inventory %s', this.id );
    this.continuesINV = false;
    this.readTagDataAfterINV = false;
    return this.INV();
  }

  public stopContinuesINV(): Promise<void> {
    RfidReaderController.logger.info( 'stopContinuesINV %s' + this.id );
    this.continuesINV = false;
    return this.BRK();
  }

  public writeToDataToCard( tag: string, data: string ): Promise<void> {
    return new Promise( ( resolve, reject ) => {
      RfidReaderController.logger.info( 'writeToDataToCard %s, %s ', tag, data );
      this.BRK()
          .then( () => {
            this.writeTagData( tag, data )
                .then( () => {
                  resolve();
                } )
                .catch( e => reject( 'writeToDataToCard:' + e ) );
          } )
          .catch( e => void 0 );
    } );
  }

  protected initSerialPort( portPath, config: OpenOptions ) {
    RfidReaderController.logger.info( 'initSerialPort: ' + this.id + ' : ' + portPath );

    config.autoOpen = false;

    this.port = new SerialPort( portPath, config );

    /** this.port.on('error', (error: Error) => {
            RfidReaderController.logger.error('Port Error: %O', error);
            this.emit('error', error);
            // throw new Error('Port Error:' + JSON.stringify(error));
            // this.listPorts();
        }); */

    this.port.on( 'open', () => {
      this.lastLogDate = new Date();
      RfidReaderController.logger.info( 'SerialPort open' );
      this.emit( 'open' );
      this.EOF()
          .then( () => {
            // add parser after EOF
            this.parser = this.port.pipe( new Delimiter( { delimiter: '\r\n' } ) );
            this.parser.on( 'data', ( res ) => {
              // RfidReaderController.logger.info( 'parser.on data %s', res );
              if ( this.parserCallback ) {
                this.parserCallback( res );
              } else {
                RfidReaderController.logger.info( 'no parserCallback', res.toString() );
              }
            } );
            this.getRevision()
                .then( () => {
                  this.readerInitialised = true;
                  RfidReaderController.logger.info( 'Reader Initialised' );
                  this.emit( 'ready' );
                } )
                .catch( e => {
                  this.tryPortReOpen();
                  RfidReaderController.logger.error( e );
                } );
          } )
          .catch( e => {
            RfidReaderController.logger.error( 'no Reader detected', e );
            this.emit( 'error', 'no Reader detected at Port: ' + portPath );
            this.tryPortReOpen();
            /** this.disconnect()
             .then(() => void 0)
             .catch(e => RfidReaderController.logger.error(e)); */
          } );
    } );

    this.port.on( 'close', () => {
      this.emit( 'close' );
    } );

    this.openPort();
  }

  protected openPort() {
    RfidReaderController.logger.info( 'openPort()' );
    this.port.open( ( error: Error ) => {
      if ( error ) {
        RfidReaderController.logger.error( 'Port Error: %O', error );
        this.emit( 'error', error );
        this.tryPortReOpen();
      }
    } );
  }

  protected tryPortReOpen() {
    RfidReaderController.logger.info( 'tryPortReOpen()' );
    this.invIsRunning = false;
    this.parserCallback = void 0;
    this.lastTags = {};

    this.BRK()
        .then( () => {
          this.closePort()
              .then( () => {
                RfidReaderController.logger.info( 'tryPortReOpen() => closePort() => then()' );
                setTimeout( () => this.openPort(), 2000 );
              } )
              .catch( ( e ) => {
                RfidReaderController.logger.info( 'tryPortReOpen() => closePort() => error(%O)', e );
                setTimeout( () => this.openPort(), 2000 );
              } );
        } );
  }

  /**
   * EOF
   * EOF This command turns on the End of Frame Delimiter (EOF). This means that after every complete message (frame) the
   * last CR will be followed by an additional line feed (LF, 0x0A). This allows the user to build simpler parsers since it
   * is clear when no to expect any further message from the reader.
   * */

  protected EOF(): Promise<void> {
    return new Promise( ( resolve, reject ) => {
      RfidReaderController.logger.info( 'EOF' );
      const command = 'EOF';

      const timeout = setTimeout( () => {
        RfidReaderController.logger.info( 'EOF:Timeout port.removeListener => readable' );
        this.port.removeListener( 'readable', onEOF );
        reject( 'EOF:Timeout' );
      }, RfidReaderController.commandTimeoutMS );

      const onEOF = () => {
        const resString = this.port.read().toString();
        RfidReaderController.logger.trace( 'onEOF res:%s', resString );
        if ( resString.indexOf( 'OK' ) > -1 ) {
          RfidReaderController.logger.trace( 'onEOF res:%s', resString );
          this.port.removeListener( 'readable', onEOF );
          clearTimeout( timeout );
          resolve();
        } else if ( resString.indexOf( 'UPA' ) > -1 ) {
          this.port.removeListener( 'readable', onEOF );
          clearTimeout( timeout );
          reject( 'EOF:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
        } else if ( resString.indexOf( 'IVF' ) > -1 ) {
          clearTimeout( timeout );
          this.BRK().then( () => reject( 'EOF:received IVF' ) );
        } else {
          this.port.removeListener( 'readable', onEOF );
          clearTimeout( timeout );
          reject( 'EOF:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
        }
      };

      this.port.on( 'readable', onEOF );
      this.sendSerialCommand( command );

    } );
  }

  /**
   * REV
   * The revision command requests the device type and hard- and software revision of the reader. The reader returns its device
   * type and it’s hard- and software revision. Revision has no parameters and returns no error codes.
   * */

  protected getRevision(): Promise<void> {
    return new Promise( ( resolve, reject ) => {
      RfidReaderController.logger.trace( 'getRevision' );

      const timeout = setTimeout( () => {
        this.parserCallback = void 0;
        reject( 'getRevision:Timeout' );
      }, RfidReaderController.commandTimeoutMS );

      this.parserCallback = ( res ) => {
        const resString = res.toString();
        RfidReaderController.logger.trace( 'onREV: %s %o', this.id, resString );
        if ( resString.indexOf( 'IVF' ) > -1 ) {
          this.BRK().then( () => reject( 'getRevision:received IVF' ) );
        } else {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          resolve();
        }
      };
      const command = 'REV';
      this.sendSerialCommand( command );

    } );
  }

  /**
   * BRK
   * Breaks INVon Error
   * */
  protected BRK(): Promise<void> {
    RfidReaderController.logger.info( 'BRK' );
    const command = 'BRK';
    this.sendSerialCommand( command );
    return Promise.resolve();
  }

  public invIsRunning: boolean = false;

  protected runINV() {
    RfidReaderController.logger.trace( 'runINV %s', this.id );

    if ( this.invTimeout ) {
      clearInterval( this.invTimeout );
      this.invTimeout = null;
    }
    if ( this.continuesINV === true ) {
      RfidReaderController.logger.trace( 'runINV setInterval checkINVAlive' );
      this.invIsRunning = true;
      this.invTimeout = setInterval( () => {
        this.checkINVAlive()
      }, RfidReaderController.checkINVAliveIntervalMS );
    }

    const restartCNTINV = ( e ) => {
      if ( e !== null ) {
        RfidReaderController.logger.error( 'runINV INV.catch CNTINV %O', e );
      }
      if ( this.continuesINV === true ) {
        setTimeout( () => {
          RfidReaderController.logger.info( 'runINV:restartCNTINV' );
          this.runINV();
        }, RfidReaderController.restartINVTimeoutMS );
      }
    };

    this.INV()
        .then( ( tags: ITags ) => {
          RfidReaderController.logger.info( 'runINV INV.then tags %o', tags );
          this.checkINVChanges( tags )
              .then( ( res ) => {
                RfidReaderController.logger.trace( 'runINV checkINVChanges.then %o', res );
                restartCNTINV( null );
              } )
              .catch( ( e ) => restartCNTINV( e ) );
        } )
        .catch( e => restartCNTINV( e ) );
  }

  /**
   *
   INV
   This command returns all UIDs from ISO/IEC 14443-1 to 3 compatible transponder, which are in the read range of the reader.
   Only single and double UIDs are supported (all types of ISO14443A known today).
   The length of the response can either be 4 bytes (single) or 7 bytes (double).
   Contineous Read (CNR)
   To allow the repeated / continuous execution of all commands, the “CNR” prefix was implemented in the firmware. Any one tag (or reader)
   command can be written after the “CNR“ prefix and will then be repeated indefinitely or until the “BRK” command is sent (see below).
   This is a very powerful mechanism for unassisted operations where the reader is initialized at the beginning and then repeats the
   command over and over. Examples for useful continuous operations are reading / searching for tag IDs, reading data from tags or even
   writing and locking data on tags continuously, e.g. in a printer.
   Optional Parameter: ONT (Only New Tags)
   Using this parameter, the reader will only report new tag ids to the host so you don't have to filter for already known tags. As long as
   a card/transponder stays in the field (and is powered) it will not respond a second time. Using other commands (like RDT or WDT) or
   starting the INV command again will reset the reader field and therefore also the tags so they will report again during the next cycle.
   Optional Parameter: BAR (Break At Read)
   To automatically break a continuous command with the first successful execution – in the above example with the first inventory run that
   finds at least one tag – use the BAR parameter. This saves having to use BRK when having found a tag.
   Example: Wait silently for a tag to enter the field, report its ID and then stop. For this to be silent, VBL should be set to 0 (see
   Verbosity Level).
   * */

  protected lastInvTime: Date = new Date();

  protected invTimeout;

  protected checkINVAlive() {
    if ( this.invIsRunning === true ) {

      const invTime: Date = new Date();
      const invDiff = invTime.getTime() - this.lastInvTime.getTime();

      RfidReaderController.logger.trace( 'checkINVAlive invDiff:%d', invDiff );

      if ( invDiff > RfidReaderController.invDeadTimeoutMS ) {
        RfidReaderController.logger.error( 'INV is dead %d', invDiff );
        this.invIsRunning = false;
        this.tryPortReOpen();
      }
    } else {
      RfidReaderController.logger.info( 'checkINVAlive invIsRunning %s', this.invIsRunning );
    }
  }

  protected INV(): Promise<ITags> {
    return new Promise( ( resolve, reject ) => {
      RfidReaderController.logger.trace( 'INV' );

      this.parserCallback = ( res ) => {
        // RfidReaderController.logger.trace('INV parserCallback', res.toString());
        this.lastInvTime = new Date();

        const tags = {};

        let resString = res.toString();
        // RfidReaderController.logger.trace( 'onINV: %s', resString );
        if ( resString.indexOf( 'IVF 00' ) > -1 ) {
          this.checkINVChanges( {} )
              .then( ( res ) => void 0 )
              .catch( ( e ) => {
                reject( 'INV:' + e );
              } );
        } else {

          let INVArray: string[] = _.compact( resString.split( '\r' ) );
          resString = null;
          if ( INVArray && INVArray.length > 0 ) {
            // write tag IDs
            INVArray.forEach( ( invItem, index ) => {
              invItem = invItem.split( '\n' ).join( '' ).split( '\r' ).join( '' );
              if ( invItem.indexOf( 'BRA' ) > -1 ) {
                // BRA comes when CNR INV found a Tag
                RfidReaderController.logger.trace( 'onINV set BRK' );
                // this.parserCallback = void 0;
                resolve( tags );
              } else {
                if ( invItem.indexOf( 'IV' ) === -1 && invItem.indexOf( 'IF' ) === -1 && invItem.indexOf( 'BRA' ) === -1 ) {
                  // tagid
                  tags[ invItem ] = invItem;
                  RfidReaderController.logger.trace( 'tag: %s', invItem );
                }
              }
            } );
            INVArray = null;
          }
        }
      };

      let command = 'CNR INV BAR';

      this.sendSerialCommand( command );
    } );
  }

  protected checkINVChanges( tags: ITags ): Promise<ITags> {

    if ( tags === this.lastTags ) {
      RfidReaderController.logger.trace( 'tags === this.lastTag %o', tags );
      return Promise.resolve( tags );
    }

    const tagIDs: string[] = _.keys( tags );
    const lastTagIDs: string[] = _.keys( this.lastTags );

    // check tag removes
    lastTagIDs.forEach( lastTag => {
      if ( tagIDs.indexOf( lastTag ) === -1 ) {
        let eventData = {
          id: this.id,
          tag: lastTag
        };
        RfidReaderController.logger.trace( 'tagRemove %o', eventData );
        this.emit( RfidReaderController.tagRemove, eventData );
        eventData = null;
      }
    } );

    // check tag adds
    const addedArray = tagIDs.filter( tag => lastTagIDs.indexOf( tag ) === -1 );

    this.lastTags = tags;

    if ( addedArray && addedArray.length > 0 ) {
      RfidReaderController.logger.trace( 'checkINVChanges => addedArray : %O', addedArray );
      return this.readTags( addedArray );
    } else {
      RfidReaderController.logger.trace( 'checkINVChanges => no changes tags: %O', tags );
      return Promise.resolve( tags );
    }
  }

  protected readTags( tagIDs: string[] ): Promise<ITags> {
    return new Promise( ( resolve, reject ) => {
      const tags = {};
      let nextTag: string;
      const getTagData = () => {
        RfidReaderController.logger.trace( 'readTags => getTagData' );
        nextTag = tagIDs.shift();
        if ( nextTag ) {
          if ( this.readTagDataAfterINV === false ) {
            // skip read by setting tagCache
            this.tagCache[ nextTag ] = nextTag;
          }
          if ( this.tagCache[ nextTag ] ) {
            // skip SEL => AUT => RDT
            RfidReaderController.logger.trace( 'use Data from Cache %s : %s', nextTag, this.tagCache[ nextTag ] );
            tags[ nextTag ] = this.tagCache[ nextTag ];
            const eventData: IRFIDEvent = {
              id: this.id,
              tag: nextTag,
              data: this.tagCache[ nextTag ]
            };
            this.emit( RfidReaderController.tagAdd, eventData );
            // RfidReaderController.logger.trace( 'readTags => on onRDTFinished tagAdd [%o]', eventData );
            if ( tagIDs.length > 0 ) {
              getTagData();
            } else {
              tags[ nextTag ] = this.tagCache[ nextTag ];
              RfidReaderController.logger.trace( 'readTags => Finished tags [%o]', tags );
              resolve( tags );
            }
          } else {
            this.readTagData( nextTag )
                .then( ( tagData: string ) => {
                  // RfidReaderController.logger.trace( 'readTagData then: %O', tagData );
                  tags[ nextTag ] = tagData;
                  this.tagCache[ nextTag ] = tagData;
                  const eventData: IRFIDEvent = {
                    id: this.id,
                    tag: nextTag,
                    data: tagData
                  };
                  this.emit( RfidReaderController.tagAdd, eventData );
                  // RfidReaderController.logger.trace( 'readTags => on onRDTFinished tagAdd [%o]', eventData );
                  if ( tagIDs.length > 0 ) {
                    getTagData();
                  } else {
                    tags[ nextTag ] = this.tagCache[ nextTag ];
                    RfidReaderController.logger.trace( 'readTags => Finished tags [%o]', tags );
                    resolve( tags );
                  }
                } )
                .catch( ( e ) => {
                  this.lastTags = {};
                  reject( 'readTags:' + e );
                } );
          }
        }
      };
      getTagData();
    } );
  }

  /**
   Memory Organisation
   The memory of MiFare classic chips is organized in sectors composed of several numbers of data blocks and one trailer. MiFare 1K
   chips contain 16 sectors of 3 data blocks and one (the fourth) trailer (Table 14). For the lower 32 sector of MiFare 4K chips
   the same applies. The higher 8 sectors are composed of 15 data blocks and one (the 16th) trailer block(Table 15). All blocks
   are read-/writable only if the corresponding sector was successfully authenticated.
   The Trailer Block:
   The trailer contains two secret keys (A and B) to authenticate the corresponding sector and information about access rights
   (the access bits). Teh trailer block is always the last block in a sector. This means that each sector can have its own keys
   for giving write or read access.
   Data blocks:
   The data blocks contain 16 read-/writable bytes depending on the access bits in the sector trailer, except block 0 in sector 0,
   which is a read-only manufacturer block. These blocks can configured as normal read/write blocks or as value blocks.
   Sector 0 in Block 0 is the manufacturer block
   */

  protected getnextBlockNumber( block ) {
    let nextBlock = block + 1;
    const trailerBlockIndex = this.trailerblocks.indexOf( nextBlock );
    if ( trailerBlockIndex > -1 ) {
      nextBlock = nextBlock + 1; // skip TrailerBlock
    }
    return nextBlock;
  }

  public readTagData( tag: string ): Promise<string> {
    return new Promise( ( resolve, reject ) => {

      let currentBlockNumber = 1; // Sector 0 in Block 0 is the manufacturer block
      let currentdataBlocks = '';

      let readAttempt: number = 0;
      const tryRead = ( e ) => {
        if ( readAttempt < RfidReaderController.maxReadAttempts ) {
          readAttempt++;
          RfidReaderController.logger.trace( 'readTagData => tryRead readAttempt: %d', readAttempt );
          // reset vars
          currentBlockNumber = 1;
          currentdataBlocks = '';
          this.selectTag( tag )
              .then( () => autAndReadBlock( 1 ) )
              .catch( ( e ) => {
                RfidReaderController.logger.warn( 'readTagData => tryRead readAttempt: %d failed: %O', readAttempt, e );
                setTimeout( () => tryRead( 'readTagData:' + e ), RfidReaderController.rwRetryTimeout );
              } );
        } else {
          RfidReaderController.logger.error( 'readTagData => tryRead readAttempt: %d failed giving up: %O', readAttempt, e );
          readAttempt = 0;
          reject( 'readTagData:' + e );
        }
      };

      const autAndReadBlock = ( block: number ) => {
        this.authenticateBlock( block )
            .then( ( block ) => {
              this.readBlock( block )
                  .then( ( blockData ) => {
                    currentdataBlocks += blockData;
                    RfidReaderController.logger.trace( 'onRDT currentdataBlocks block: %s', blockData );
                    currentBlockNumber = this.getnextBlockNumber( currentBlockNumber );
                    if ( currentBlockNumber < RfidReaderController.myfareNumberOfBlocks && blockData
                      !== RfidReaderController.myfareEmptyBlock ) {
                      autAndReadBlock( currentBlockNumber );
                    } else {
                      resolve( RfidReaderController.hex_to_ascii( currentdataBlocks ) );
                    }
                  } )
                  .catch( ( e ) => setTimeout( () => tryRead( 'readTagData:' + e ), RfidReaderController.rwRetryTimeout ) );
            } )
            .catch( ( e ) => setTimeout( () => tryRead( 'readTagData:' + e ), RfidReaderController.rwRetryTimeout ) );
      };

      tryRead( null );

    } );
  }

  public writeTagData( tag: string, data: string ): Promise<void> {
    return new Promise( ( resolve, reject ) => {

      let currentBlock = 1; // Sector 0 in Block 0 is the manufacturer block
      let writedataBlocks: { [ key: string ]: string } = {};

      // split data into 16Bit blocks
      const hexData: string = RfidReaderController.ascii_to_hex( data );
      const numBlocks = Math.ceil( hexData.length / 32 );

      for ( let i = 0; i < this.dataBlocks.length; i++ ) {
        let blockData = hexData.substr( (i) * 32, 32 );
        if ( blockData.length < 32 ) {
          blockData = _.padEnd( blockData, 32, '0' );
        }
        const nextBlockNumber: string = this.dataBlocks[ i ].toString();
        writedataBlocks[ nextBlockNumber ] = blockData;
      }
      const numWriteBlocks = _.keys( writedataBlocks ).length;

      RfidReaderController.logger.trace( 'writedataBlocks: %O', writedataBlocks );
      RfidReaderController.logger.trace( 'numBlocks: %d numWriteBlocks: %d', numBlocks, numWriteBlocks );

      let writeAttempt: number = 0;
      const tryWrite = ( e ) => {

        if ( e ) {
          RfidReaderController.logger.warn( 'writeTagData => tryWrite error: %O', e );
        }

        if ( writeAttempt < RfidReaderController.maxWriteAttempts ) {
          writeAttempt++;
          RfidReaderController.logger.trace( 'writeTagData => tryWrite writeAttempt: %d', writeAttempt );
          // reset vars
          currentBlock = 1;
          this.selectTag( tag )
              .then( () => autAndWriteBlock( 1 ) )
              .catch( ( selError ) => {
                RfidReaderController.logger.warn( 'writeTagData => tryWrite writeAttempt: %d failed: %O', writeAttempt,
                  selError );
                setTimeout( () => tryWrite( 'writeTagData:' + selError ), RfidReaderController.rwRetryTimeout );
              } );
        } else {
          RfidReaderController.logger.error( 'writeTagData => tryWrite writeAttempt: %d failed giving up: %O', writeAttempt, e );
          writeAttempt = 0;
          reject( 'writeTagData:' + e );
        }
      };

      const autAndWriteBlock = ( block: number ) => {
        this.authenticateBlock( block )
            .then( ( block ) => {
              this.writeBlock( block, writedataBlocks[ block ] )
                  .then( ( res ) => {
                    RfidReaderController.logger.trace( 'autAndWriteBlock res: %s', res );
                    currentBlock = this.getnextBlockNumber( currentBlock );
                    if ( currentBlock <= numWriteBlocks ) {
                      autAndWriteBlock( currentBlock );
                    } else {
                      this.tagCache[ tag ] = data;
                      resolve();
                    }
                  } )
                  .catch( ( e ) => setTimeout( () => tryWrite( 'writeTagData:' + e ), RfidReaderController.rwRetryTimeout ) );
            } )
            .catch( ( e ) => setTimeout( () => tryWrite( 'writeTagData:' + e ), RfidReaderController.rwRetryTimeout ) );
      };

      tryWrite( null );
    } );
  }

  /**
   * SEL Select Tag
   * Before you can exchange data with a MiFare chip, the transponder has to be activated (or „selected“ in the ISO14443
   * language). There are two different modes to select a card. Manual Transponder Select (MTS), which needs the UID of the
   * transponder
   * (via a previous INV command) or Automatic Transponder Select (ATS).
   * **/

  protected selectTag( tag: string ): Promise<void> {
    return new Promise( ( resolve, reject ) => {

      RfidReaderController.logger.trace( 'selectTag' );

      const timeout = setTimeout( () => {
        this.parserCallback = void 0;
        reject( 'selectTag: Timeout' );
      }, RfidReaderController.commandTimeoutMS );

      this.parserCallback = ( res ) => {
        const resString = res.toString();
        RfidReaderController.logger.trace( 'onSEL: %o', resString );
        if ( resString.indexOf( 'UPA' ) > -1 || resString.indexOf( 'TNR' ) > -1 || resString.indexOf( 'EHX' ) > -1 ) {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          reject( 'selectTag:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
        } else {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          resolve();
        }
      };

      // Selects a tag with Manual Transponder Select
      const command = 'SEL MTS ' + tag;
      this.sendSerialCommand( command );
    } );
  }

  /**
   * Authentication (AUT)
   * In order to read or write data from or to MiFare classic chips, the respective memory block has to be previously
   * authenticated with a key. The key can either be selected by using the SKU command (set key to use) or can be directly given
   * as a parameter when using the direct (DRT) parameter (direct mode)
   * **/

  protected authenticateBlock( block?: number ): Promise<number> {
    return new Promise( ( resolve, reject ) => {

      if ( !block ) {
        block = 1;
      }

      // RfidReaderController.logger.trace( 'authenticateBlock block: %d', block );

      this.parserCallback = ( res ) => {
        const resString = res.toString();

        const timeout = setTimeout( () => {
          this.parserCallback = void 0;
          reject( 'authenticateBlock: Timeout Block No.' + block );
        }, RfidReaderController.commandTimeoutMS );

        // UPA<CR> Unknown parameter
        // BIH<CR>Block no. is too high (i.e. bigger than 63 at MiFare 1k)
        // ATE<CR>Authentication Error (i.e. wrong key)
        // NKS<CR>No Key Select, select a temporary or a static key (use STK or SSK)
        // CNS<CR>Card is Not Selected
        if ( resString.indexOf( 'UPA' ) > -1 || resString.indexOf( 'BIH' ) > -1 || resString.indexOf( 'ATE' ) > -1
          || resString.indexOf( 'NKS' ) > -1 || resString.indexOf( 'CNS' ) > -1 ) {
          RfidReaderController.logger.error( 'on authenticateBlock: Block No. %d "%s"', block,
            RfidReaderController.codes.error[ resString ] );
          this.parserCallback = void 0;
          clearTimeout( timeout );
          reject( 'authenticateBlock:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
        } else {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          resolve( block );
        }
      };
      // AUT<SPACE>DRT<SPACE>[Key]<SPACE>{Type}<SPACE>[Block No.]<CR>
      // Direct authentication of block 1 (sector 0) with keytype A and key FFFFFFFFFFFF
      const command = 'AUT DRT ' + RfidReaderController.myfareKey + ' ' + RfidReaderController.myfareKeyType + ' ' + block;
      // RfidReaderController.logger.trace( 'authenticateBlock block %d command: %s', block, command );
      this.sendSerialCommand( command );
    } );
  }

  /**
   * RDT
   *
   * Read Data from Tag (RDT)
   * The read data command is used to retrieve the data stored in a transponder. Normally it returns 16 bytes. For compatibility
   * to other ISO/IEC 14443-1 to 4 transponder than MiFare classic, it has a direct read mode, marked with the first parameter
   * “DRT”. In this mode the second parameter is the custom command.
   *
   * Additionally, this command supports the ability to read multiple blocks with one command, i.e. parameter “ALL” for all
   * blocks of a sector, or “CNT” for a variable block count.
   *
   * If MiFare classic is used, the block has to be authenticated first (see the AUT command in the next chapter). The command
   * returns all blocks from a sector. If MiFare 4K is used, parameter “All” is set and the authenticated block no. is higher
   * than 127 it returns
   * 16 blocks. For Custom-Read-Commands the length of a response is maximal 64 bytes.
   *
   * **/

  protected readBlock( block?: number ): Promise<string> {
    return new Promise( ( resolve, reject ) => {

      // RfidReaderController.logger.trace( 'readBlock No. %d', block );
      const timeout = setTimeout( () => {
        this.parserCallback = void 0;
        reject( 'readBlock: Timeout' );
      }, RfidReaderController.commandTimeoutMS );

      this.parserCallback = ( res ) => {
        let resString = res.toString();
        // RfidReaderController.logger.trace( 'onReadBlock: %o', resString );

        // UPA<CR> Unknown parameter
        // EDX<CR>A decimal parameter includes non decimal characters
        // BAE<CR> Block no. not readable, i.e. wrong key
        // BNA<CR> Block no. not authenticated (only MiFare classic)
        // NMA<CR>No MiFare chip 1k or 4k authenticated (only ALL-Mode)
        // NB0<CR>Number of blocks to Read is 0

        if ( resString.indexOf( 'UPA' ) > -1 || resString.indexOf( 'EDX' ) > -1 || resString.indexOf( 'BAE' ) > -1
          || resString.indexOf( 'BNA' ) > -1 || resString.indexOf( 'NMA' ) > -1 || resString.indexOf( 'NB0' ) > -1 ) {
          RfidReaderController.logger.error( 'onReadBlock: Block No. %d "%s"', block,
            RfidReaderController.codes.error[ resString ] );
          this.parserCallback = void 0;
          clearTimeout( timeout );
          reject( 'readBlock:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
        } else {
          this.parserCallback = void 0;
          clearTimeout( timeout );
          resolve( resString.split( '\n' ).join( '' ).split( '\r' ).join( '' ) );
        }
      };

      //  RfidReaderController.logger.trace( 'setRDT' );
      const command = 'RDT ' + block;
      this.sendSerialCommand( command );

    } );
  }

  /**
   * Write Data to Tag (WDT)
   * The write data command normally stores 16 bytes of data into a block (data or trailer block). For compatibility to other
   * ISO/IEC 14443-1 to 4 transponder than MiFare classic, the command also has a direct write mode, marked with the first
   * parameter “DRT”. The number of bytes will not be checked in this mode and it depends on the second parameter (Data).
   *
   * To write to MiFare Ultralight cards (which only have four bytes per block) the first parameter becomes “W4”. This parameter
   * writes
   * 4 bytes to the card.
   *
   * The selected block has to be writable for this command to work.
   *
   * ATTENTION
   * If you write wrong data to the trailer block of a sector (the fourth block of every sector, e.g. block 3, 7, 11, etc.), the
   * sector may become locked forever or be even unreadable afterwards. We recommend to use the STM command to change the
   * information in the trailer blocks and don't write data to it directly (although it is possible).
   *
   */
  protected writeBlock( block: number, data: string ): Promise<string> {
    return new Promise( ( resolve, reject ) => {

      RfidReaderController.logger.trace( 'writeBlock No. %d data: %s  length:%d', block, data, data.length );

      if ( this.dataBlocks.indexOf( block ) > -1 ) {

        const timeout = setTimeout( () => {
          this.parserCallback = void 0;
          reject( 'writeBlock: Timeout' );
        }, RfidReaderController.commandTimeoutMS );

        this.parserCallback = ( res ) => {

          let resString = res.toString();
          RfidReaderController.logger.trace( 'writeBlock: %o', resString );

          // UPA<CR> Unknown parameter
          // EHX<CR>The string cannot be interpreted as valid data or contains non hex characters
          // BAE<CR> Block no. not readable, i.e. wrong key, see Block– and Access Mode
          // BNA<CR> Block no. not authenticated (only MiFare classic)
          // NMA<CR>No MiFare chip 1k or 4k authenticated (only ALL-Mode)
          // WDL<CR>The hex string does not have the correct length (i.e. 16 bytes in normal mode)

          if ( resString.indexOf( 'UPA' ) > -1 || resString.indexOf( 'EHX' ) > -1 || resString.indexOf( 'BAE' ) > -1
            || resString.indexOf( 'BNA' ) > -1 || resString.indexOf( 'NMA' ) > -1 || resString.indexOf( 'WDL' ) > -1 ) {
            RfidReaderController.logger.error( 'writeBlock: Block No. %d "%s"', block,
              RfidReaderController.codes.error[ resString ] );
            this.parserCallback = void 0;
            clearTimeout( timeout );
            reject( 'writeBlock:' + resString + ':' + RfidReaderController.codes.error[ resString ] );
          } else {
            this.parserCallback = void 0;
            clearTimeout( timeout );
            resolve( resString );
          }
        };

        const command = 'WDT ' + data + ' ' + block;
        this.sendSerialCommand( command );

      } else {
        reject( 'writeBlock:"' + block + '" is not a Datablock!' );
      }
    } );
  }

  protected sendSerialCommand( val: string ) {
    // write buffer to serialPort
    //  RfidReaderController.logger.trace( 'sendSerialCommand:%s', val );
    this.writeAndDrain( Buffer.from( val + '\r' ), ( error?: Error ) => {
      if ( error ) {
        RfidReaderController.logger.error( 'Error on write: %o', error );
        //  this.emit( 'error', error );
      }
    } );
  }

  protected flush( callback ) {
    if ( this.port.isOpen ) {
      this.port.flush( callback );
    } else {
      callback( new Error( 'Flush => Port is not open!' ) );
    }
  }

  protected writeAndDrain( data, callback ) {
    if ( this.port.isOpen ) {
      if ( this.port.write( data ) ) {
        callback( null );
      } else {
        this.port.drain( callback );
      }
    } else {
      callback( new Error( 'write => Port is not open!' ) );
    }
  }

  public disconnect(): Promise<void> {
    RfidReaderController.logger.info( 'disconnect: %s' + this.id );
    return this.resetReader()
               .then( () => this.BRK() )
               .then( () => this.closePort() )
               .catch( () => this.closePort() );
  }

  protected closePort(): Promise<void> {
    return new Promise( ( resolve, reject ) => {
      RfidReaderController.logger.info( 'closePort: %s' + this.id );

      if ( this.parser ) {
        this.port.unpipe();
        this.parser.removeAllListeners();
      }

      this.port.close( ( error: Error ) => {
        if ( error ) {
          RfidReaderController.logger.error( 'Error on close: %O ', error );
          reject( 'closePort:' + error );
        } else {
          resolve();
        }
      } );
    } );
  }
}
